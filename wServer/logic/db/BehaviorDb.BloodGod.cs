﻿#region Logic Behaviors Used
using wServer.logic.behaviors;
using wServer.logic.loot;
using wServer.logic.transitions;
#endregion

namespace wServer.logic
{
    partial class BehaviorDb
    {
        _ BloodGod = () => Behav()
        .Init("Blood God",
            new State(
                new ConditionalEffect(ConditionEffectIndex.DazedImmune, true),
                new ConditionalEffect(ConditionEffectIndex.ParalyzeImmune, true),
                new ConditionalEffect(ConditionEffectIndex.StasisImmune, true),
                new StayCloseToSpawn(1, 10),
                new ConditionalEffect(ConditionEffectIndex.StunImmune, true),
                new State("1",
                    new PlayerWithinTransition(7.5, "StartTheSwain")
                ),
                new State("StartTheSwain",
                    new Wander(1.5),
                    new Taunt("TGV0J3Mgc3RhcnQgdGhpcyBmaWdodC4uLiBvaCBtYWFhYWFuLCBpJ3ZlIGJlZW4gd2FpdGluZyBzaW5jZSBXRUVLUywgbGl0ZXJhbGx5IGR1ZGUh"),//B64
                    new Shoot(15, 2, shootAngle: 23, projectileIndex: 0, coolDown: 1900),
                    new Shoot(15, 2, shootAngle: 23, projectileIndex: 0, fixedAngle: 270, coolDown: 2500, coolDownOffset: 1200),
                    new Shoot(15, 2, shootAngle: 23, projectileIndex: 0, fixedAngle: 180, coolDown: 2500, coolDownOffset: 1100),
                    new Shoot(15, 2, shootAngle: 23, projectileIndex: 0, fixedAngle: 90, coolDown: 2500, coolDownOffset: 900),
                    new Shoot(15, 2, shootAngle: 23, projectileIndex: 0, fixedAngle: 0, coolDown: 2500, coolDownOffset: 1000),
                    new HpLessTransition(65000, "Swalla")
                ),
                new State("Swalla",
                    new Wander(1.5),
                    new Taunt("RWgsIHlvdSdyZSBnb29kIGF0IGZpZ2h0aW5nLCBidXQgd2VsbCwgYXJlIHlvdSBHT09EIGVOb1VHaD8="),//B64
                    new Shoot(15, 6, shootAngle: 25, projectileIndex: 1, fixedAngle: 270, coolDown: 2800, coolDownOffset: 1300),
                    new Shoot(15, 6, shootAngle: 25, projectileIndex: 0, fixedAngle: 180, coolDown: 2800, coolDownOffset: 1200),
                    new Shoot(15, 6, shootAngle: 25, projectileIndex: 1, fixedAngle: 90, coolDown: 2800, coolDownOffset: 1000),
                    new Shoot(15, 6, shootAngle: 25, projectileIndex: 0, fixedAngle: 0, coolDown: 2800, coolDownOffset: 1100),
                    new HpLessTransition(30000, "SweetA")
                ),
                new State("SweetA",
                    new Wander(1.5),
                    new Taunt("T2theSwgdGhpcyBpcyBnb2luZyBmYXIgZnJvbSB3aGF0IGkndmUgd2FudGVkIHRvIGZpZ2h0LCB0aW1lIHRvIGdvIHN0cm9uZyBvbiB5b3Uu"),//B64
                    new Shoot(15, 6, shootAngle: 25, projectileIndex: 1, coolDown: 1500),
                    new Shoot(3, 8, 36, projectileIndex: 2, coolDown: 1000), 
                    new HpLessTransition(10000, "SweetB")
                ),
                new State("SweetB",
                    new ConditionalEffect(ConditionEffectIndex.ArmorBroken, true),//just to make it EZ'IER
                    new Flash(0xfFF0000, 2.5, 9000001),
                    new Wander(1.5),
                    new Taunt("T2ggbm8sIGFtIGkgZ29ubmEgZGllLCBib3NzPyBHZXQgbWUgc3Ryb25nZXIgbm93IQ=="),//B64 
                    new Shoot(15, 6, shootAngle: 25, projectileIndex: 1, coolDown: 1500),
                    new Shoot(15, 6, shootAngle: 25, projectileIndex: 1, fixedAngle: 270, coolDown: 2000, coolDownOffset: 1300),
                    new Shoot(15, 6, shootAngle: 25, projectileIndex: 0, fixedAngle: 180, coolDown: 2000, coolDownOffset: 1200),
                    new Shoot(15, 6, shootAngle: 25, projectileIndex: 1, fixedAngle: 90, coolDown: 2000, coolDownOffset: 1000),
                    new Shoot(15, 6, shootAngle: 25, projectileIndex: 0, fixedAngle: 0, coolDown: 2000, coolDownOffset: 1100),
                    new Shoot(3, 8, 36, projectileIndex: 2, coolDown: 1000)
                )
            ),
        #region T13s
            new OnlyOneMostDamager(15,
                new ItemLoot("Bow of Mystical Energy", 0.1),
                new ItemLoot("Staff of the Vital Unity", 0.1),
                new ItemLoot("Dagger of Sinister Deeds", 0.1),
                new ItemLoot("Wand of Evocation", 0.1),
                new ItemLoot("Sword of Splendor", 0.1)
                ),
        #endregion

            new MostDamagers(8,
                new ItemLoot("Potion of Dexterity", 0.66),
                new ItemLoot("Potion of Wisdom", 0.88),
                new ItemLoot("Potion of Defense", 0.74),
                new ItemLoot("Potion of Vitality", 0.88)
            ),
            new MostDamagers(20,
                new ItemLoot("Potion of Speed", 0.33),
                new ItemLoot("Potion of Wisdom", 0.33),
                new ItemLoot("Potion of Vitality", 0.33),
                new ItemLoot("Health Potion", 0.33),
                new ItemLoot("Magic Potion", 0.33)
            )
        )

        #region Minions
        .Init("worker mage",
            new State(
                new StayCloseToSpawn(1, 6),
                new ConditionalEffect(ConditionEffectIndex.StunImmune, true),
                new State("1",
                    new PlayerWithinTransition(9.2, "waitorders1")
                ),
                new State("waitorders1",
                    new Wander(0.5),
                    new Taunt("God damn it! What's the meaning of this?! Warriors, ATTACK!"),
                    new Order(7.5, "worker warrior", "fight1"),
                    new ConditionalEffect(ConditionEffectIndex.Invulnerable),
                    new TimedTransition(1500, "attack1")
                    ),
                new State("attack1",
                    new Wander(0.5),
                    new Shoot(10, predictive: 1, coolDown: 1200),
                    new ConditionalEffect(ConditionEffectIndex.Invulnerable),
                    new EntityNotExistsTransition("worker warrior", 12, "awaken, knight")
                    ),
                new State("heal-w",
                    new Wander(0.5),
                    new Taunt("Eh? Alreadly hurt?!"),
                    new SpecificHeal(5, 1200, "devil2", 2500),
                    new ConditionalEffect(ConditionEffectIndex.Invulnerable)
                    ),
                new State("stop-w",
                    new Wander(0.5),
                    new Shoot(10, predictive: 1, coolDown: 1200),
                    new ConditionalEffect(ConditionEffectIndex.Invulnerable),
                    new EntityNotExistsTransition("worker warrior", 12, "awaken, knight")
                    ),
                new State("awaken, knight",
                    new Wander(0.5),
                    new Order(7, "worker knight", "fight1"),
                    new Shoot(10, predictive: 1, coolDown: 1200),
                    new ConditionalEffect(ConditionEffectIndex.Invulnerable),
                    new EntityNotExistsTransition("worker knight", 12, "solo meme")
                    ),
                new State("heal-k",
                    new Wander(0.5),
                    new Taunt("Oh no... you too! Useless crap!"),
                    new SpecificHeal(5, 1600, "devil3", 2500),
                    new ConditionalEffect(ConditionEffectIndex.Invulnerable)
                    ),
                new State("stop-k",
                    new Wander(0.5),
                    new Shoot(10, predictive: 1, coolDown: 1200),
                    new ConditionalEffect(ConditionEffectIndex.Invulnerable),
                    new EntityNotExistsTransition("worker knight", 12, "solo meme")
                    ),
                new State("solo meme",
                    new Follow(0.6, 20, 5),
                    new Remeff(ConditionEffectIndex.Invulnerable),
                    new Shoot(10, count: 3, shootAngle: 10, predictive: 1, coolDown: 1200),
                    new Taunt("They died... haha,let's fight now!")
                )
            ),
            new MostDamagers(3,
                new ItemLoot("Potion of Wisdom", 0.09),
                new ItemLoot("Health Potion", 0.17)
            )
        )
        .Init("worker knight",
            new State(
                new StayCloseToSpawn(1, 6),
                new ConditionalEffect(ConditionEffectIndex.StunImmune, true),
                new State("1",
                    new PlayerWithinTransition(9.2, "waitorders1")
                ),
                new State("waitorders1",
                    new Orbit(0.27, 2.5, target: "worker mage"),
                    new ConditionalEffect(ConditionEffectIndex.Invulnerable)
                ),
                new State("fight1",
                    new Remeff(ConditionEffectIndex.Invulnerable),
                    new Follow(0.4, 20, 2.2),
                    new Shoot(4.8, count: 6, projectileIndex: 1, shootAngle: 18, predictive: 1, coolDown: 3000),
                    new Shoot(6, count: 4, shootAngle: 7, predictive: 1, coolDown: 1100),
                    new HpLessTransition(4700, "waitorders2")
                ),
                new State("waitorders2",
                    new ReturnToSpawn(true, 1),
                    new Orbit(0.27, 2.5, target: "worker mage"),
                    new Order(10, "worker mage", "heal-k"),
                    new TimedTransition(5000, "fight2"),
                    new ConditionalEffect(ConditionEffectIndex.Invulnerable)
                ),
                new State("fight2",
                    new Taunt("..."),
                    new Remeff(ConditionEffectIndex.Invulnerable),
                    new Order(7, "worker mage", "stop-k"),
                    new Flash(0xfFF0000, 1.3, 9000001),
                    new Follow(0.4, 20, 2.2),
                    new ConditionalEffect(ConditionEffectIndex.Berserk),
                    new Shoot(4.8, count: 6, shootAngle: 18, projectileIndex: 1, predictive: 1, coolDown: 1800),
                    new Shoot(6, count: 2, shootAngle: 6, predictive: 1, coolDown: 1000)
                )
            ),
            new MostDamagers(3,
                new ItemLoot("Potion of Defense", 0.07),
                new ItemLoot("Health Potion", 0.17)
            )
        )
        .Init("worker warrior",
            new State(
                new StayCloseToSpawn(1, 6),
                new ConditionalEffect(ConditionEffectIndex.StunImmune, true),
                new State("1",
                    new PlayerWithinTransition(9.2, "waitorders1")
                ),
                new State("waitorders1",
                    new Orbit(0.27, 4, target: "worker mage"),
                    new ConditionalEffect(ConditionEffectIndex.Invulnerable)
                ),
                new State("fight1",
                    new Taunt("Ok, Sir!"),
                    new Follow(0.4, 20, 2.2),
                    new Remeff(ConditionEffectIndex.Invulnerable),
                    new Shoot(4.8, count: 6, shootAngle: 18, predictive: 1, coolDown: 3300),
                    new Shoot(6, count: 2, shootAngle: 6, predictive: 1, coolDown: 1300),
                    new HpLessTransition(3700, "waitorders2")
                ),
                new State("waitorders2",
                    new ReturnToSpawn(true, 1),
                    new Order(7.5, "worker mage", "heal-w"),
                    new Orbit(0.27, 4, target: "worker mage"),
                    new ConditionalEffect(ConditionEffectIndex.Invulnerable),
                    new TimedTransition(5000, "fight2")
                ),
                new State("fight2",
                    new Order(7, "worker mage", "stop-w"),
                    new Taunt("Hmph, enough of this!"),
                    new Remeff(ConditionEffectIndex.Invulnerable),
                    new Follow(0.4, 20, 2.2),
                    new Flash(0xfFF0000, 1.3, 9000001),
                    new ConditionalEffect(ConditionEffectIndex.Berserk),
                    new Shoot(4.8, count: 6, shootAngle: 18, predictive: 1, coolDown: 2000),
                    new Shoot(6, count: 2, shootAngle: 6, predictive: 1, coolDown: 1070)
                )
            ),
            new MostDamagers(5,
                new ItemLoot("Potion of Speed", 0.07),
                new ItemLoot("Health Potion", 0.17)
            )
        )
        #endregion
;
}
}
