﻿#region Logic Behaviors Used
using wServer.logic.behaviors;
using wServer.logic.loot;
using wServer.logic.transitions;
#endregion

namespace wServer.logic
{
    partial class BehaviorDb
    {
        _ Megaman = () => Behav()
        .Init("Megaman",
            new State(
                new ConditionalEffect(ConditionEffectIndex.DazedImmune, true),
                new ConditionalEffect(ConditionEffectIndex.StasisImmune, true),
                new StayCloseToSpawn(0.55, 25),
                new ConditionalEffect(ConditionEffectIndex.StunImmune, true),
                new State("1",
                    new ConditionalEffect(ConditionEffectIndex.Invulnerable, true),
                    new PlayerWithinTransition(20, "2.1")
                ),
                new State("2.1",
                    new Taunt("I AM MEGAMAN!"),
                    new InvisiToss("MegamanPinkThing", 0, 0, coolDown: 190),
                    new InvisiToss("MegamanShield", 0, 0, coolDown: 250),
                    new TimedTransition(1000, "2")
                    ),
                new State("2",
                    new Remeff(ConditionEffectIndex.Invulnerable, true),
                    new Wander(0.4),
                    new Shoot(15, 2, shootAngle: 10, projectileIndex: 0, predictive: 1, coolDown: 1300),
                    new HpLessTransition(110000, "3")
                ),
                new State("3",
                    new Wander(0.4),
                    new Shoot(15, 12, shootAngle: 30, projectileIndex: 1, coolDown: 5500, coolDownOffset: 4500),
                    new Shoot(15, 2, shootAngle: 10, projectileIndex: 0, coolDown: 3000, predictive: 1, coolDownOffset: 2000),
                    new Shoot(15, 2, shootAngle: 10, projectileIndex: 0, coolDown: 3000, predictive: 1, coolDownOffset: 2600),
                    new HpLessTransition(60000, "4")
                ),
                new State("4",
                    new Reproduce("MegamanPinkThing", 1, 1, 1, 20000),
                    new Reproduce("MegamanShield", 1, 1, 1, 30000),
                    new Wander(0.55),
                    new Shoot(15, 12, shootAngle: 30, projectileIndex: 0, coolDown: 4000, coolDownOffset: 2800),
                    new Shoot(15, 2, shootAngle: 10, projectileIndex: 1, predictive: 1, coolDown: 2200),
                    new Shoot(15, 1, projectileIndex: 2, predictive: 1, coolDown: 1400)
                )
            ),

            new OnlyOneMostDamager(1,
                new ItemLoot("Mega Blaster", 0.05),
                new TierLoot(14, ItemType.Armor, 0.4),
                new TierLoot(13, ItemType.Weapon, 0.4)
                ),
            new MostDamagers(10,
                new TierLoot(10, ItemType.Weapon, 0.2),
                new TierLoot(11, ItemType.Armor, 0.2),
                new TierLoot(4, ItemType.Ring, 0.2),
                new ItemLoot("Potion of Attack", 0.05),
                new ItemLoot("Potion of Wisdom", 0.05),
                new ItemLoot("Potion of Defense", 0.05),
                new ItemLoot("Potion of Vitality", 0.05),
                new ItemLoot("Greater Health Potion", 0.25),
                new ItemLoot("Greater Magic Potion", 0.25)
            ),
            new MostDamagers(6,
                new TierLoot(11, ItemType.Weapon, 0.1),
                new TierLoot(12, ItemType.Armor, 0.1),
                new TierLoot(5, ItemType.Ring, 0.1),
                new TierLoot(5, ItemType.Ability, 0.15)
            ),
            new MostDamagers(3,
                new TierLoot(12, ItemType.Weapon, 0.05),
                new TierLoot(13, ItemType.Armor, 0.05),
                new TierLoot(56, ItemType.Ability, 0.05)
            )
       )

 
            .Init("MegamanShield", //Megaman S.H.I.E.L.D. agent   btw
            new State(
                new EntityNotExistsTransition("Megaman", 30, "Rage"),
                new StayCloseToSpawn(0.35, 12),
                new ConditionalEffect(ConditionEffectIndex.DazedImmune, true),
                new ConditionalEffect(ConditionEffectIndex.StunImmune, true),
                new State("1",
                    new Wander(0.3),
                    new ConditionalEffect(ConditionEffectIndex.Invulnerable, true),
                    new PlayerWithinTransition(25, "2")
                ),
                new State("2",
                    new SpecificHeal(10, 300, "Self", 2000),
                    new Wander(0.3),
                    new Protect(0.35, "Megaman", 30, 5.5, 5.5),
                    new Shoot(15, 2, shootAngle: 9, predictive: 1, coolDown: 4000),
                    new Remeff(ConditionEffectIndex.Armored, true),
                    new ConditionalEffect(ConditionEffectIndex.Invulnerable, true),
                    new TimedTransition(4000, "3")
                ),
                new State("3",
                    new Wander(0.3),
                    new Protect(0.35, "Megaman", 30, 5.5, 5.5),
                    new Shoot(15, 2, shootAngle: 9, predictive: 1, coolDown: 1000),
                    new ConditionalEffect(ConditionEffectIndex.Armored, true),
                    new Remeff(ConditionEffectIndex.Invulnerable, true),
                    new TimedTransition(5000, "2")
                ),
                new State("Rage",
                    new Follow(0.65, 30, range: 1.5),
                    new Remeff(ConditionEffectIndex.Invulnerable, true),
                    new Shoot(15, 2, shootAngle: 9, predictive: 1, coolDown: 500)
                )
            ),
            new Threshold(1.3,
                new ItemLoot("Magic Potion", 0.75),
                new ItemLoot("Health Potion", 0.75)
          )
        )


        .Init("MegamanPinkThing",
            new State(
                new EntityNotExistsTransition("Megaman", 30, "Rage"),
                new StayCloseToSpawn(0.4, 12),
                new ConditionalEffect(ConditionEffectIndex.DazedImmune, true),
                new ConditionalEffect(ConditionEffectIndex.StunImmune, true),
                new State("1",
                    new Wander(0.45),
                    new ConditionalEffect(ConditionEffectIndex.Invulnerable, true),
                    new PlayerWithinTransition(25, "2")
                ),
                new State("2",
                    new Wander(0.45),
                    new Remeff(ConditionEffectIndex.Invulnerable, true),
                    new Protect(0.4, "Megaman", 30, 4.5, 4.5),
                    new Shoot(15, 4, shootAngle: 9, predictive: 1, coolDown: 2300)
                ),
                new State("Rage",
                    new Follow(0.65, 30, range: 3.5),
                    new Remeff(ConditionEffectIndex.Invulnerable, true),
                    new Shoot(15, 4, shootAngle: 9, predictive: 1, coolDown: 1150)
                )
            ),
            new Threshold(1.3,
                new ItemLoot("Magic Potion", 0.75),
                new ItemLoot("Health Potion", 0.75)
          )
        )

        ;
    }
}