﻿#region Logic Behaviors Used (L.B.U.)
using wServer.logic.behaviors;
using wServer.logic.loot;
using wServer.logic.transitions;
#endregion

namespace wServer.logic
{
    partial class BehaviorDb
    {
        _ Megaman2 = () => Behav()
        #region BubbleMan
        .Init("frog looking thingy",
            new State(
                new ConditionalEffect(ConditionEffectIndex.ParalyzeImmune, true),
                new ConditionalEffect(ConditionEffectIndex.StasisImmune, true),
                new StayCloseToSpawn(0.5, 15),
                new ConditionalEffect(ConditionEffectIndex.StunImmune, true),
                new State("1",
                    new PlayerWithinTransition(12.5, "CTRLC+CTRLV")
                ),
                new State("CTRLC+CTRLV",
                    new Shoot(10, 10, shootAngle: 36, fixedAngle: 180, projectileIndex: 0, coolDown: 1200),
                    new Shoot(10, 4, shootAngle: 8, projectileIndex: 0, coolDown: 1600),
                    new Wander(0.25),
                    new Flash(0x0300e1, 0.5, 9000001),
                    new HpLessTransition(50000, "N")
                ),
                new State("N",
                    new Spawn("Bubble Shield", 1, 1, 1000),
                    new Shoot(10, 10, shootAngle: 36, fixedAngle: 180, projectileIndex: 1, coolDown: 1200),
                    new Shoot(10, 4, shootAngle: 8, projectileIndex: 1, coolDown: 1600),
                    new Wander(0.25),
                    new Flash(0x69ff00, 0.7, 9000001),
                    new HpLessTransition(35000, "Ni")
                ),
                new State("Ni",
                    new Spawn("Bubble Shield", 1, 1, 1000),
                    new Shoot(10, 10, shootAngle: 36, fixedAngle: 180, projectileIndex: 1, coolDown: 1200),
                    new Shoot(10, 4, shootAngle: 8, projectileIndex: 2, coolDown: 1600),
                    new Wander(0.25),
                    new Flash(0xffd600, 0.9, 9000001),
                    new HpLessTransition(15000, "Nitro")
                ),
                new State("Nitro",
                    new Spawn("Bubble Shield", 1, 1, 1000),
                    new Shoot(10, 10, shootAngle: 36, fixedAngle: 180, projectileIndex: 2, coolDown: 1200),
                    new Shoot(10, 4, shootAngle: 8, projectileIndex: 2, coolDown: 1600),
                    new Wander(0.25),
                    new Flash(0xe50000, 1.2, 9000001)
                )
            ),
            new OnlyOneMostDamager(1,
                new ItemLoot("Bow of Mystical Energy", 0.1),
                new ItemLoot("Staff of the Vital Unity", 0.1),
                new ItemLoot("Dagger of Sinister Deeds", 0.1),
                new ItemLoot("Wand of Evocation", 0.1),
                new ItemLoot("Sword of Splendor", 0.1)
            ),
            new OnlyOneMostDamager(1,
                new ItemLoot("Magic Antimatter Creator!!!", 0.01)
            ),
            new MostDamagers(5,
                new ItemLoot("Potion of Speed", 0.34),
                new ItemLoot("Potion of Mana", 0.2),
                new ItemLoot("Potion of Defense", 0.31),
                new ItemLoot("Potion of Vitality", 0.31)
                )
            )
        .Init("Bubble Shield",
            new State(
                new StayCloseToSpawn(0.4, 12),
                    new ConditionalEffect(ConditionEffectIndex.Invulnerable, true),
                new ConditionalEffect(ConditionEffectIndex.DazedImmune, true),
                new ConditionalEffect(ConditionEffectIndex.StunImmune, true),
                new State("1",
                    new PlayerWithinTransition(25, "2")
                ),
                new State("2",
                    new EntityNotExistsTransition("frog looking thingy", 15, "death"),
                    new Orbit(1.3, 2.5, 15, "frog looking thingy"),
                    new Shoot(15, 3, shootAngle: 15, predictive: 1, coolDown: 2500)
                    ),
                new State("death",
                    new Suicide(
                        )
                )
            ),
            new Threshold(1.3,
                new ItemLoot("Magic Potion", 0.75),
                new ItemLoot("Health Potion", 0.75)
          )
        )

        #endregion


        #region triple gay below
;
    }
}
#endregion