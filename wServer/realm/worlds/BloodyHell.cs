﻿namespace wServer.realm.worlds
{
    public class BloodyHell : World
    {
        public BloodyHell()
        {
            Name = "Bloody Hell";
            ClientWorldName = "Bloody Hell";
            Background = 0;
            Difficulty = 5;
            ShowDisplays = true;
            AllowTeleport = false;
        }

        protected override void Init()
        {
            LoadMap("wServer.realm.worlds.maps.blooddungeon.jm", MapType.Json);
        }
    }
}